package com.example.proyectoexamenfinalcpa201grupono2.ui.cap7;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.proyectoexamenfinalcpa201grupono2.Imagen_Cap1_Activity;
import com.example.proyectoexamenfinalcpa201grupono2.Imagen_Cap7_Activity;
import com.example.proyectoexamenfinalcpa201grupono2.MCI_Cap1_Activity;
import com.example.proyectoexamenfinalcpa201grupono2.MCI_Cap7_Activity;
import com.example.proyectoexamenfinalcpa201grupono2.R;
import com.example.proyectoexamenfinalcpa201grupono2.VideoYT_Cap1_Activity;
import com.example.proyectoexamenfinalcpa201grupono2.VideoYT_Cap7_Activity;

public class Cap7Fragment extends Fragment {

    private Cap7ViewModel cap1ViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        cap1ViewModel =
                ViewModelProviders.of(this).get(Cap7ViewModel.class);
        View root = inflater.inflate(R.layout.cap7_fragment, container, false);
//        final TextView textView = root.findViewById(R.id.text_home);
        cap1ViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //   textView.setText(s);
            }
        });

        LinearLayout constraintLayout= root.findViewById(R.id.layoutHome);
        AnimationDrawable animationDrawable=(AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();


        Button button = (Button) root.findViewById(R.id.BVMMCIcap7);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myintent = new Intent(getActivity(), MCI_Cap7_Activity.class);
                startActivity(myintent);
            }
        });
        Button button2 = (Button) root.findViewById(R.id.BVideocap7);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myintent = new Intent(getActivity(), VideoYT_Cap7_Activity.class);
                startActivity(myintent);
            }
        });
        Button button3 = (Button) root.findViewById(R.id.ZoomCap7);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myintent = new Intent(getActivity(), Imagen_Cap7_Activity.class);
                startActivity(myintent);
            }
        });

        return root;



    }

}