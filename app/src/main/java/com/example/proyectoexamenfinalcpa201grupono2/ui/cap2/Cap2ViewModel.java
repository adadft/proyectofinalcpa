package com.example.proyectoexamenfinalcpa201grupono2.ui.cap2;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class Cap2ViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public Cap2ViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}