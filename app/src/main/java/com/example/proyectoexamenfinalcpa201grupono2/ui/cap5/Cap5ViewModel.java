package com.example.proyectoexamenfinalcpa201grupono2.ui.cap5;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class Cap5ViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public Cap5ViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}