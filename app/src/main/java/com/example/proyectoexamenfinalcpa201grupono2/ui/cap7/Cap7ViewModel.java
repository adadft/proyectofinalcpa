package com.example.proyectoexamenfinalcpa201grupono2.ui.cap7;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class Cap7ViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public Cap7ViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}