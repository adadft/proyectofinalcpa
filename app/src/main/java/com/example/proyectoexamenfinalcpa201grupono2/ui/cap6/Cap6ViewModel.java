package com.example.proyectoexamenfinalcpa201grupono2.ui.cap6;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class Cap6ViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public Cap6ViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}