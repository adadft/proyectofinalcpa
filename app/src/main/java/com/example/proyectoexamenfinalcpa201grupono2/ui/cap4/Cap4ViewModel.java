package com.example.proyectoexamenfinalcpa201grupono2.ui.cap4;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class Cap4ViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public Cap4ViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}