package com.example.proyectoexamenfinalcpa201grupono2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

public class Zoom_Cap1_Activity extends AppCompatActivity {
private ImageView imageView;
private ScaleGestureDetector detector;
private float xBegin=0;
private float yBegin=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom__cap1_);

        imageView=findViewById(R.id.IVZoomCap1);
        xBegin=imageView.getScaleX();
        yBegin=imageView.getScaleY();
        detector=new ScaleGestureDetector(this,new ScaleListener(imageView));
    }
    public void reset(View view){
        imageView.setScaleX(xBegin);
        imageView.setScaleY(yBegin);
        detector=new ScaleGestureDetector(this,new ScaleListener(imageView));
    }
    public boolean onTouchEvent(MotionEvent event){
        detector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }
}