package com.example.proyectoexamenfinalcpa201grupono2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class Video_Cap1_Activity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, YouTubePlayer.PlaybackEventListener {
    YouTubePlayerView youTubePlayerView;
    String claveYoutube="AIzaSyDR-K1dTo5eeiTj9cF96iMkKYRcNxW539s";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video__cap1);

        youTubePlayerView=findViewById(R.id.yt_cap1);
        youTubePlayerView.initialize(claveYoutube,this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean fueRestaurado) {
        if (!fueRestaurado){
            youTubePlayer.cueVideo("azxDhcKYku4");
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if(youTubeInitializationResult.isUserRecoverableError()){
            youTubeInitializationResult.getErrorDialog(this,1).show();
        }else {
            String error="Error al inicializar Youtube"+youTubeInitializationResult.toString();
           Toast.makeText(getApplication(),error,Toast.LENGTH_LONG).show();
        }
    }

    protected  void  onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode==1){
            getYouTubePlayerProvider().initialize(claveYoutube,this);
        }
    }
    protected YouTubePlayer.Provider getYouTubePlayerProvider(){
        return youTubePlayerView;
    }

    @Override
    public void onPlaying() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onStopped() {

    }

    @Override
    public void onBuffering(boolean b) {

    }

    @Override
    public void onSeekTo(int i) {

    }
}